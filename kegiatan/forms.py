from django import forms
from .models import Kegiatan

class DaftarPeserta(forms.Form):
    nama_peserta = forms.CharField(max_length=20)
class BuatKegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'nama_kegiatan',
        ]

