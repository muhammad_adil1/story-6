from .forms import BuatKegiatanForm, DaftarPeserta
from .models import Kegiatan, PesertaKegiatan
from django.shortcuts import render, get_object_or_404

def for_request_get_buat_kegiatan(nama_peserta,nama_kegiatan1,a_list):
    nama_kegiatan = nama_kegiatan1
    pecahan_nama_kegiatan = nama_kegiatan.split()
    list_holder = []
    for kata_kegiatan in pecahan_nama_kegiatan:
        list_holder.append(kata_kegiatan.capitalize())
    nama_kegiatan = ' '.join(list_holder)
    if nama_kegiatan not in a_list:
        Kegiatan.objects.create(nama_kegiatan=nama_kegiatan).pesertakegiatan_set.\
            create(nama_peserta=nama_peserta).save()
        a_list.append(nama_kegiatan)
    else:
        Kegiatan.objects.filter(nama_kegiatan__startswith=nama_kegiatan)[0]\
            .pesertakegiatan_set.create(nama_peserta=nama_peserta).save()

def create_context_query_nama_kegiatan():
    list_holder_nama_kegiatan = []
    for kegiatan in Kegiatan.objects.all():
        list_holder = []
        kegiatan = str(kegiatan)
        pecahan_nama_kegiatan = kegiatan.split()
        for kata_kegiatan in pecahan_nama_kegiatan:
            list_holder.append(kata_kegiatan.capitalize())
        nama_kegiatan = ' '.join(list_holder)
        list_holder_nama_kegiatan.append(nama_kegiatan)
    return list_holder_nama_kegiatan
        
def landing_page(request):
    form_peserta = DaftarPeserta(request.GET or None)
    buat_kegiatan = BuatKegiatanForm(request.GET or None)

    list_nama_kegiatan = create_context_query_nama_kegiatan()
    
    if request.GET:
        for_request_get_buat_kegiatan(request.GET['nama_peserta'],\
                                    request.GET['nama_kegiatan'],\
                                        list_nama_kegiatan)
    list_nama_peserta = [kegiatan.pesertakegiatan_set.all() for kegiatan in Kegiatan.objects.all()]
    output = zip(list_nama_kegiatan,list_nama_peserta)
    context = {'output':output,\
              'form_peserta':form_peserta,\
                  'buat_kegiatan': buat_kegiatan}
    return render(request, 'landing_page.html', context)