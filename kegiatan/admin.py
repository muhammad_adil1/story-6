from django.contrib import admin
from .models import Kegiatan, PesertaKegiatan
# Register your models here.

admin.site.register(Kegiatan)
admin.site.register(PesertaKegiatan)
