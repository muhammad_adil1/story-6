from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=20,default='')
    def __str__(self):
        return self.nama_kegiatan
class PesertaKegiatan(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama_peserta = models.CharField(max_length=20,default='')
    def __str__(self):
        return self.nama_peserta