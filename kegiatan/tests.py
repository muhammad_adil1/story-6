from django.test import TestCase, Client
from .models import *
from .views import *

class TesStory6(TestCase):
    def setUp(self):
        Kegiatan.objects.create(nama_kegiatan='menari').pesertakegiatan_set.create(nama_peserta='adil')
    def test_models_class_Kegiatan_objects_exist(self):
        tes = Kegiatan.objects.all().count()
        self.assertLess(0, tes)
    def test_models_class_Peserta_Kegiatan_objects_exist(self):
        tes = Kegiatan.objects.all()[0].pesertakegiatan_set.all().count()
        self.assertLess(0, tes)
    def test_models_class_Kegiatan_print_str(self):
        tes = Kegiatan.objects.all()[0]
        self.assertIsInstance(str(tes), str)
    def test_models_class_Peserta_Kegiatan_print_str(self):
        tes = Kegiatan.objects.all()[0].pesertakegiatan_set.all()[0]
        self.assertIsInstance(str(tes),str)
    def test_views_get_nama_kegiatan_case_sensitive(self):
        orang = Client()
        Kegiatan.objects.all().delete()
        Kegiatan.objects.create(nama_kegiatan='menyapu')
        response = orang.get('', {'nama_kegiatan':'menyapu', 'nama_peserta':'adil'})
        tes = Kegiatan.objects.all().count()
        self.assertEqual(1,tes)
        self.assertContains(response,'Menyapu',status_code=200)
    def test_views_get_nama_kegiatan_if_havent_exist(self):
        orang = Client()
        response = orang.get('', {'nama_kegiatan':'menyapu', 'nama_peserta':'adil'})
        tes = Kegiatan.objects.all().count()
        self.assertEqual(2,tes)
    def test_views_nama_kegiatan_exist(self):
        orang = Client()
        response = orang.get('')
        self.assertContains(response,'Menari',status_code=200)
    def test_views_nama_peserta_exist(self):
        orang = Client()
        response = orang.get('')
        self.assertContains(response,'adil',status_code=200)
    
